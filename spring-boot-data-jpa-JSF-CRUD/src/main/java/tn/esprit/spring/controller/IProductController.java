package tn.esprit.spring.controller;

import tn.esprit.spring.entity.Product;

public interface IProductController {
	 public String save();
	 public Product getProduct();
	 public String delete(int id);
	 public String modifier(Product p, String name, int price);
	 public String saveModif();
}
