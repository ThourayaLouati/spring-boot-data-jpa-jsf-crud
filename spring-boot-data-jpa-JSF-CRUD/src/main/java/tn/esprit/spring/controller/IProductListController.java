package tn.esprit.spring.controller;

import java.util.List;

import tn.esprit.spring.entity.Product;

public interface IProductListController {
	 public void loadData();
	 public List<Product> getProducts();
}
