package tn.esprit.spring.controller;


import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import tn.esprit.spring.entity.Product;
import tn.esprit.spring.repository.ProductRepository;
import tn.esprit.spring.service.IProductService;


@Scope(value = "session")
@Component(value = "productController")
@ELBeanName(value = "productController")
@Join(path = "/product", to = "/product-form.jsf")

public class ProductController implements IProductController {
    
	@Autowired
	IProductService iProductService;
    
    public String save() {
       return iProductService.save();
    }
    public Product getProduct() {
        return iProductService.getProduct();
    }
    public String delete(int id) {
    
    return iProductService.delete(id);
    }
   
    public String modifier(Product p, String name, int price) {
    	
    	return iProductService.modifier(p, p.getName(),p.getPrice());
    }
	
	public String saveModif() {
		
		return iProductService.saveModif();
	}
}