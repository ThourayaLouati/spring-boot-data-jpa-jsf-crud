package tn.esprit.spring.controller;
import org.ocpsoft.rewrite.annotation.Join;
import org.ocpsoft.rewrite.annotation.RequestAction;
import org.ocpsoft.rewrite.el.ELBeanName;
import org.ocpsoft.rewrite.faces.annotation.Deferred;
import org.ocpsoft.rewrite.faces.annotation.IgnorePostback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import tn.esprit.spring.entity.Product;
import tn.esprit.spring.repository.ProductRepository;
import tn.esprit.spring.service.IProductService;

import java.util.List;
//@Scope (value = "session")

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@Scope (value = "session")
@Component (value = "productList")
@ELBeanName(value = "productList")
@Join(path = "/", to = "/product-list.jsf")
public class ProductListController implements IProductListController{
	private List<Product> products;
	
	@Autowired
	IProductService iProductService;
	@Deferred
	@RequestAction
	@IgnorePostback
    public void loadData() {
        iProductService.loadData();
    }
    public List<Product> getProducts() {
    	products=iProductService.getProducts();
        return products;
    }
}