package tn.esprit.spring.service;



import java.util.List;

import tn.esprit.spring.entity.Product;

public interface IProductService {
	 public String save();
	 public Product getProduct();
	 public String delete(int id);
	 public String modifier(Product p, String name, int price);
	 public void loadData();
	 public List<Product> getProducts();
	 public String saveModif();

	
}
