package tn.esprit.spring.service;



import java.util.List;

import org.ocpsoft.rewrite.annotation.RequestAction;
import org.ocpsoft.rewrite.faces.annotation.Deferred;
import org.ocpsoft.rewrite.faces.annotation.IgnorePostback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.esprit.spring.entity.Product;
import tn.esprit.spring.repository.ProductRepository;

@Service

public class ProductServiceImpl implements IProductService
{   
	private Product product = new Product();
	private List<Product> products;
	@Autowired
	ProductRepository productRepository;


	public String save() {
		productRepository.save(product);
		product = new Product();
		return "/product-list.xhtml?faces-redirect=true";
	}


	public Product getProduct() {

		return product;
	}


	public String delete(int id) {
		Product p =productRepository.findById(id).get();
		productRepository.delete(p);
		return "/product-list.xhtml?faces-redirect=true";
	}


	public String modifier(Product p, String name, int price) {
		product=p;
		return "/product-form-modif.xhtml?faces-redirect=true";


	}


	public void loadData() {
		products = (List<Product>) productRepository.findAll();

	}


	public List<Product> getProducts() {
		return products;
	}



	public String saveModif() {
		Product p1=productRepository.findById(product.getId()).get();
		p1.setName(product.getName());
		p1.setPrice(product.getPrice());
		productRepository.save(p1);
		product = new Product();
		return "/product-list.xhtml?faces-redirect=true";
	}


}
